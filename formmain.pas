unit FormMain;

{$mode objfpc}{$H+}

interface

uses
  {$IfDef UNIX} clocale, {$EndIf}
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ComCtrls, Spin, CustomEditTester;

type

  { TForm1 }

  TForm1 = class(TForm)
    edTestControl: TPageControl;
    tsEdit: TTabSheet;
    tsMemo: TTabSheet;
    tsFloatSpinEdit: TTabSheet;
    Edit1: TEdit;
    Memo1: TMemo;
    FloatSpinEdit1: TFloatSpinEdit;
    Panel1: TPanel;
    edTstModified: TCheckBox;
    edTstCaretPos: TCheckBox;
    edTstSelStart: TCheckBox;
    edTstSelLength: TCheckBox;
    edTstSelText: TCheckBox;
    edTstText: TCheckBox;
    edTstValue: TCheckBox;
    btTest: TButton;
    btCheck: TButton;
    btReset: TButton;
    edKeypressDelay: TCheckBox;
    edLog: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edTestControlChange(Sender: TObject);
    procedure edTstChange(Sender: TObject);
    procedure btTestClick(Sender: TObject);
    procedure btCheckClick(Sender: TObject);
    procedure btResetClick(Sender: TObject);
  private
    FTester: TCustomEditTester;
    procedure OnLog({%H-}ASender: TCustomEditTester; const ALine: string);
    procedure OnKeyPress11802(Sender: TObject; var Key: char);
    procedure OnChange16678(Sender: TObject);
    procedure OnChange24371(Sender: TObject);
    procedure OnEditingDone21224(Sender: TObject);
    procedure OnChangeSpinSet(Sender: TObject);
    procedure OnChange32602(Sender: TObject);
  public
    procedure FocusEdit(AEnabled: Boolean);
    function GetTestSubjects(): TTestSubjects;
    procedure EditRunTests();
    procedure MemoRunTests();
    procedure SpinEditRunTests();
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses
{$IfDef LCLGTK2}
  glib2, gtk2,
{$EndIf}
  InterfaceBase, LCLPlatformDef, Clipbrd, LCLType, Math, FormSpinTest;

const
  Edit1ResetText = 'Edit1';
  Memo1ResetText = 'Memo1';
  LBL = Length(sLineBreak);

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  Caption := 'LCLPlatform: ' + LCLPlatformDirNames[WidgetSet.LCLPlatform];
  {$IfDef LCLGTK2}
  Caption := Format('%s, gtk version: %d.%d.%d, glib version: %d.%d.%d',
    [Caption,
      gtk_major_version, gtk_minor_version, gtk_micro_version,
      glib_major_version, glib_minor_version, glib_micro_version]);
  {$EndIf}
  FTester := TCustomEditTester.Create();
  FTester.OnLog := @OnLog;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FTester.Free();
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  edTestControl.TabIndex := 2;
  edTestControlChange(edTestControl);
  btReset.Click();
end;

procedure TForm1.edTestControlChange(Sender: TObject);
var
  TS: TTestSubjects;
begin
  TS := GetTestSubjects();
  case( edTestControl.TabIndex )of
    0: begin
      FTester.TestEdit := Edit1;
      FTester.ResetText := Edit1ResetText;
      FTester.TestSubjects := TS - [tsValue];
    end;
    1: begin
      FTester.TestEdit := Memo1;
      FTester.ResetText := Memo1ResetText;
      FTester.TestSubjects := TS - [tsValue];
    end;
    2: begin
      FTester.TestEdit := FloatSpinEdit1;
      FTester.ResetText := '0';
      FTester.TestSubjects := TS;
    end;
  end;
  FTester.ResetEdit();
end;

procedure TForm1.edTstChange(Sender: TObject);
begin
  edTestControlChange(edTestControl);
end;

procedure TForm1.btTestClick(Sender: TObject);
begin
  btTest.Enabled := False;
  try
    edLog.Lines.Clear();
    FTester.KeyPressDelay := ifthen(edKeypressDelay.Checked, 200, 20);
    FTester.Start();
    case( edTestControl.TabIndex )of
      0: EditRunTests();
      1: MemoRunTests();
      2: SpinEditRunTests();
    end;
    FocusEdit(True);
    FTester.ResetEdit();
    FTester.LogSummary();
  finally
    btTest.Enabled := True;
  end;
end;

procedure TForm1.btCheckClick(Sender: TObject);
begin
  FTester.RecordState(tpCheckPoint);
end;

procedure TForm1.btResetClick(Sender: TObject);
begin
  FTester.Start();
  FTester.TestBegin('');
  edLog.Lines.Clear();
end;

procedure TForm1.OnLog(ASender: TCustomEditTester; const ALine: string);
begin
  edLog.Lines.Add(ALine);
  edLog.SelStart := 100500;
end;

procedure TForm1.OnKeyPress11802(Sender: TObject; var Key: char);
begin
  case( Key )of
    '1': begin
      TEdit(Sender).SelStart := 2;
      TEdit(Sender).SelLength := 3;
      Key := #0;
    end;
    '2': begin
      TEdit(Sender).SelStart := 2;
      Key := #0;
    end;
    '3': begin
      TEdit(Sender).SelLength := 4;
      Key := #0;
    end;
    '4': begin
      TEdit(Sender).SelLength := 2;
      Key := #0;
    end;
  end;
end;

procedure TForm1.OnChange16678(Sender: TObject);
begin
  TEdit(Sender).SelStart := 4;
end;

procedure TForm1.OnChange24371(Sender: TObject);
begin
  with( TEdit(Sender) )do begin
    Text := 'ABCDEF';
    SelStart := 1;
    SelLength := Length(Text);
  end;
end;

procedure TForm1.OnEditingDone21224(Sender: TObject);
begin
  FTester.CheckPoint();
end;

procedure TForm1.OnChangeSpinSet(Sender: TObject);
begin
  TCustomFloatSpinEdit(Sender).Value := 5.0;
end;

procedure TForm1.OnChange32602(Sender: TObject);
begin
  TEdit(Sender).Text:='from-on-change';
end;

procedure TForm1.FocusEdit(AEnabled: Boolean);
begin
  FTester.SetFocus := AEnabled;
  if( not AEnabled )then begin
    edLog.SetFocus();
    Application.ProcessMessages();
  end;
end;

function TForm1.GetTestSubjects(): TTestSubjects;
begin
  Result := [tsTestProbe];
  if( edTstModified.Checked )then Include(Result, tsModified);
  if( edTstCaretPos.Checked )then Include(Result, tsCaretPos);
  if( edTstSelStart.Checked )then Include(Result, tsSelStart);
  if( edTstSelLength.Checked )then Include(Result, tsSelLength);
  if( edTstSelText.Checked )then Include(Result, tsSelText);
  if( edTstText.Checked )then Include(Result, tsText);
  if( edTstValue.Checked )then Include(Result, tsValue);
end;

procedure TForm1.EditRunTests();
var
  Focus: Integer;
  FocusStr: string;
begin
  with( FTester )do begin
    for Focus := 0 to 1 do begin
      FocusEdit(Focus = 1);
      FocusStr := ' (focus: ' + BoolToStr(Focus = 1, True) + ')';

      TestBegin('code: select text' + FocusStr);
      TestEdit.SelStart := 3;
      CheckPoint();
      TestEdit.SelStart := 0;
      CheckPoint();
      TestEdit.SelStart := 2;
      CheckPoint();
      TestEdit.SelLength := 3;
      CheckPoint();
      TestEdit.CaretPos := Point(1, 0);
      CheckPoint();
      TestEdit.CaretPos := Point(0, 0);
      CheckPoint();
      TestEdit.SelLength := 2;
      CheckPoint();
      TestEnd([
        TestState(tpCheckPoint, False, Point(3, 0), 3, 0, '', 'Edit1'),
        TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Edit1'),
        TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Edit1'),
        TestState(tpCheckPoint, False, Point(5, 0), 2, 3, 'it1', 'Edit1'),
        TestState(tpCheckPoint, False, Point(1, 0), 1, 0, '', 'Edit1'),
        TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Edit1'),
        TestState(tpCheckPoint, False, Point(2, 0), 0, 2, 'Ed', 'Edit1'),
        TestState(tpAfterTest,  False, Point(2, 0), 0, 2, 'Ed', 'Edit1')]);

      TestBegin('code: assign' + FocusStr);
      TestEdit.SelStart := 3;
      TestEdit.SelLength := 2;
      CheckPoint();
      TestEdit.Text := 'test';
      TestEnd([
        TestState(tpCheckPoint, False, Point(5, 0), 3, 2, 't1', 'Edit1'),
        TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'test'),
        TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', 'test')]);

      TestBegin('code: insert' + FocusStr);
      TestEdit.SelStart := 0;
      CheckPoint();
      TestEdit.SelText := 'xxx';
      TestEnd([
        TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Edit1'),
        TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'xxxEdit1'),
        TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'xxxEdit1')]);

      TestBegin('code: insert-end' + FocusStr);
      TestEdit.SelStart := 5;
      TestEdit.SelLength := 0;
      CheckPoint();
      TestEdit.SelText := 'xxx';
      TestEnd([
        TestState(tpCheckPoint, False, Point(5, 0), 5, 0, '', 'Edit1'),
        TestState(tpOnChange,   True,  Point(8, 0), 8, 0, '', 'Edit1xxx'),
        TestState(tpAfterTest,  True,  Point(8, 0), 8, 0, '', 'Edit1xxx')]);

      TestBegin('code: insert-uppercase' + FocusStr, '');
      TEdit(TestEdit).CharCase := ecUppercase;
      TestEdit.Text := 'Edit1';
      TestEdit.SelStart := 4;
      TestEdit.SelLength := 0;
      TestEdit.SelText := 'or';
      TestEnd([
        TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'EDIT1'),
        TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'EDITOR1'),
        TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'EDITOR1')]);
      TEdit(TestEdit).CharCase := ecNormal;

      TestBegin('code: delete' + FocusStr);
      TestEdit.SelStart := 1;
      TestEdit.SelLength := 2;
      CheckPoint();
      TestEdit.SelText := '';
      TestEnd([
        TestState(tpCheckPoint, False, Point(3, 0), 1, 2, 'di', 'Edit1'),
        TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Et1'),
        TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'Et1')]);

      TestBegin('code: replace' + FocusStr);
      TestEdit.SelStart := 0;
      TestEdit.SelLength := 3;
      CheckPoint();
      TestEdit.SelText := 'abc';
      TestEnd([
        TestState(tpCheckPoint, False, Point(3, 0), 0, 3, 'Edi', 'Edit1'),
        TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'abct1'),
        TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'abct1')]);

      TestBegin('code: replace-insert' + FocusStr);
      TestEdit.SelStart := 1;
      TestEdit.SelLength := 1;
      CheckPoint();
      TestEdit.SelText := 'x';
      CheckPoint();
      TestEdit.SelText := 'x';
      TestEnd([
        TestState(tpCheckPoint, False, Point(2, 0), 1, 1, 'd', 'Edit1'),
        TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Exit1'),
        TestState(tpCheckPoint, True,  Point(2, 0), 2, 0, '', 'Exit1'),
        TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'Exxit1'),
        TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'Exxit1')]);

      TestBegin('code: replace-same' + FocusStr);
      TestEdit.SelStart := 1;
      TestEdit.SelLength := 3;
      CheckPoint();
      TestEdit.SelText := 'dit';
      TestEnd([
        TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'dit', 'Edit1'),
        TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', 'Edit1'),
        TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', 'Edit1')]);
    end;


    FocusEdit(True);

    TestBegin('keyboard: insert');
    KeyboardSelect(0, 0);
    KeyboardPress(VK_A);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'aEdit1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'aEdit1')]);

    TestBegin('keyboard: replace');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardPress(VK_X);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'dit', 'Edit1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Ex1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Ex1')]);

    TestBegin('keyboard: replace-same');
    KeyboardSelect(1, 1);
    CheckPoint();
    KeyboardPress(VK_D);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 1, 1, 'd', 'Edit1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Edit1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Edit1')]);

    TestBegin('keyboard: backspace');
    KeyboardSelect(2, 0);
    CheckPoint();
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Edit1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Eit1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'Eit1')]);

    TestBegin('keyboard: backspace-selected');
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'it', 'Edit1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Ed1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Ed1')]);

    TestBegin('keyboard: delete');
    KeyboardSelect(2, 0);
    CheckPoint();
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Edit1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Edt1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Edt1')]);

    TestBegin('keyboard: delete-selected');
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'it', 'Edit1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Ed1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Ed1')]);

    TestBegin('keyboard: cut');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_X);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'dit', 'Edit1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'E1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'E1')]);

    TestBegin('keyboard: copy-paste');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(3, 0);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'dit', 'Edit1'),
      TestState(tpCheckPoint, False, Point(3, 0), 3, 0, '', 'Edit1'),
      TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'Ediditt1'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'Ediditt1')]);

    TestBegin('keyboard: copy-paste-selected');
    KeyboardSelect(4, -3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'dit', 'Edit1'),
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'it', 'Edit1'),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', 'Eddit1'),
      TestState(tpAfterTest,  True,  Point(5, 0), 5, 0, '', 'Eddit1')]);

    TestBegin('keyboard: copy-paste-selected-all');
    KeyboardSelect(0, 5);
    KeyboardShortcut([ssCtrl], VK_C);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 0, 5, 'Edit1', 'Edit1'),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', 'Edit1'),
      TestState(tpAfterTest,  True,  Point(5, 0), 5, 0, '', 'Edit1')]);


    FocusEdit(True);

    TestBegin('issue-7243', '');
    KeyboardPress(VK_1);
    KeyboardPress(VK_2);
    KeyboardPress(VK_3);
    KeyboardPress(VK_BACK);
    KeyboardPress(VK_BACK);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '12'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '123'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '12'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '1'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', ''),
      TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', '')]);

    TestBegin('issue-8491');
    KeyboardSelect(0, 0);
    KeyboardPress(VK_BACK);
    KeyboardSelect(5, 0);
    KeyboardPress(VK_DELETE);
    CheckPoint();
    KeyboardSelect(1, -1);
    KeyboardPress(VK_BACK);
    KeyboardSelect(3, 1);
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 5, 0,  '', 'Edit1'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0,  '', 'dit1'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0,  '', 'dit'),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0,  '', 'dit')]);

    TestBegin('issue-11802', '123456789');
    TestEdit.OnKeyPress := @OnKeyPress11802;
    KeyboardPress(VK_1);
    CheckPoint();
    KeyboardPress(VK_2);
    CheckPoint();
    KeyboardPress(VK_1);
    KeyboardPress(VK_3);
    CheckPoint();
    KeyboardPress(VK_1);
    KeyboardPress(VK_4);
    CheckPoint();
    TestEdit.OnKeyPress := nil;
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 2, 3, '345', '123456789'),
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', '123456789'),
      TestState(tpCheckPoint, False, Point(6, 0), 2, 4, '3456', '123456789'),
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, '34', '123456789'),
      TestState(tpAfterTest,  False, Point(4, 0), 2, 2, '34', '123456789')]);

    TestBegin('issue-16678');
    TestEdit.SelStart := 0;
    TestOnChange := @OnChange16678;
    KeyboardPress(VK_2);
    KeyboardPress(VK_3);
    TestOnChange := nil;
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '2Edit1'),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', '2Edi3t1'),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', '2Edi3t1')]);

    TestBegin('issue-19220');
    ResetEdit('TFooBar');
    KeyboardSelect(1, 3);
    KeyboardPress(VK_BACK);
    ResetEdit('TFooBar');
    KeyboardSelect(1, 3);
    KeyboardPress(VK_DELETE);
    ResetEdit('TFooBar');
    KeyboardSelect(1, 3);
    KeyboardPress(VK_X);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'TBar'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'TBar'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'TxBar'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'TxBar')]);

    TestBegin('issue-19239', '');
    TEdit(TestEdit).CharCase := ecUppercase;
    KeyboardPress(VK_A);
    KeyboardPress(VK_L);
    KeyboardPress(VK_E);
    KeyboardPress(VK_X);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'A'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'AL'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'ALE'),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', 'ALEX'),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', 'ALEX')]);
    TEdit(TestEdit).CharCase := ecNormal;

    TestBegin('issue-23131');
    ResetEdit('1234567890');
    KeyboardSelect(9, 0);
    KeyboardPress(VK_BACK);
    ResetEdit('1234567890');
    KeyboardSelect(6, 0);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(8, 0), 8, 0, '', '123456780'),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', '123457890'),
      TestState(tpAfterTest,  True,  Point(5, 0), 5, 0, '', '123457890')]);

    TestBegin('issue-24371');
    TestOnChange := @OnChange24371;
    KeyboardShortcut([ssCtrl], VK_A);
    CheckPoint();
    KeyboardPress(VK_X);
    TestOnChange := nil;
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 0, 5, 'Edit1', 'Edit1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'x'),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'ABCDEF'),
      TestState(tpAfterTest,  False, Point(6, 0), 1, 5, 'BCDEF', 'ABCDEF')]);

    TestBegin('issue-30596');
    ResetEdit('Edit1dafdgdfgjxczdffjkjgfe');
    KeyboardSelect(21, 0);
    KeyboardPress(VK_BACK, 4);
    KeyboardSelect(10, 0);
    KeyboardPress(VK_BACK, 4);
    TestEnd([
      TestState(tpOnChange,   True,  Point(20,0), 20, 0, '', 'Edit1dafdgdfgjxczdffkjgfe'),
      TestState(tpOnChange,   True,  Point(19,0), 19, 0, '', 'Edit1dafdgdfgjxczdfkjgfe'),
      TestState(tpOnChange,   True,  Point(18,0), 18, 0, '', 'Edit1dafdgdfgjxczdkjgfe'),
      TestState(tpOnChange,   True,  Point(17,0), 17, 0, '', 'Edit1dafdgdfgjxczkjgfe'),
      TestState(tpOnChange,   True,  Point(9, 0), 9,  0, '', 'Edit1dafddfgjxczkjgfe'),
      TestState(tpOnChange,   True,  Point(8, 0), 8,  0, '', 'Edit1dafdfgjxczkjgfe'),
      TestState(tpOnChange,   True,  Point(7, 0), 7,  0, '', 'Edit1dadfgjxczkjgfe'),
      TestState(tpOnChange,   True,  Point(6, 0), 6,  0, '', 'Edit1ddfgjxczkjgfe'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6,  0, '', 'Edit1ddfgjxczkjgfe')]);

    TestBegin('issue-32321');
    TestEdit.SelStart := 0;
    TestEdit.SelLength := Length(TestEdit.Text);
    KeyboardPress(VK_A);
    KeyboardPress(VK_B);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'a'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'ab'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'ab')]);

    TestBegin('issue-32602');
    TestOnChange := @OnChange32602;
    TestEdit.Text:='test';
    TestOnChange := nil;
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'test'),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'from-on-change'),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', 'from-on-change')]);

    TestBegin('issue-37116');
    TestEdit.SelStart := 5;
    TestEdit.SelLength := 0;
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_BACK);
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 5, 0, '', 'Edit1'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', ''),
      TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', '')]);
  end;
end;

procedure TForm1.MemoRunTests();
var
  i: Integer;
begin
  with( FTester )do begin
    FocusEdit(True);

    TestBegin('code: select text');
    TestEdit.SelStart := 3;
    CheckPoint();
    TestEdit.SelStart := 0;
    CheckPoint();
    TestEdit.SelStart := 2;
    CheckPoint();
    TestEdit.SelLength := 3;
    CheckPoint();
    TestEdit.CaretPos := Point(1, 0);
    CheckPoint();
    TestEdit.CaretPos := Point(0, 0);
    CheckPoint();
    TestEdit.SelLength := 2;
    CheckPoint();
    TestEnd([
      TestState(tpCheckPoint, False, Point(3, 0), 3, 0, '', 'Memo1'),
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Memo1'),
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Memo1'),
      TestState(tpCheckPoint, False, Point(5, 0), 2, 3, 'mo1', 'Memo1'),
      TestState(tpCheckPoint, False, Point(1, 0), 1, 0, '', 'Memo1'),
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Memo1'),
      TestState(tpCheckPoint, False, Point(2, 0), 0, 2, 'Me', 'Memo1'),
      TestState(tpAfterTest,  False, Point(2, 0), 0, 2, 'Me', 'Memo1')]);

    TestBegin('code: assign');
    TestEdit.SelStart := 1;
    TestEdit.SelLength := 3;
    CheckPoint();
    TestEdit.Text := 'test';
    CheckPoint();
    TestEdit.Text := 'test1';
    TestEdit.Text := '';
    TestEdit.Text := 'test';
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'test'),
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'test'),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'test1'),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', ''),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'test'),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', 'test')]);

    TestBegin('code: insert');
    TestEdit.SelStart := 0;
    TestEdit.SelLength := 0;
    CheckPoint();
    TestEdit.SelText := 'xxx';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Memo1'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'xxxMemo1'),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'xxxMemo1')]);

    TestBegin('code: insert-uppercase', '');
    TEdit(TestEdit).CharCase := ecUppercase;
    TestEdit.Text := 'Memo1';
    TestEdit.SelStart := 4;
    TestEdit.SelLength := 0;
    TestEdit.SelText := 'ry';
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', 'MEMO1'),
      TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'MEMORY1'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'MEMORY1')]);
    TEdit(TestEdit).CharCase := ecNormal;

    TestBegin('code: delete');
    TestEdit.SelStart := 1;
    TestEdit.SelLength := 2;
    CheckPoint();
    TestEdit.SelText := '';
    TestEnd([
      TestState(tpCheckPoint, False, Point(3, 0), 1, 2, 'em', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Mo1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'Mo1')]);

    TestBegin('code: replace');
    TestEdit.SelStart := 0;
    TestEdit.SelLength := 3;
    CheckPoint();
    TestEdit.SelText := 'abc';
    TestEnd([
      TestState(tpCheckPoint, False, Point(3, 0), 0, 3, 'Mem', 'Memo1'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', 'o1'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'abco1'),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'abco1')]);

    TestBegin('code: replace-insert');
    TestEdit.SelStart := 1;
    TestEdit.SelLength := 1;
    CheckPoint();
    TestEdit.SelText := 'x';
    CheckPoint();
    TestEdit.SelText := 'x';
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 1, 1, 'e', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Mmo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Mxmo1'),
      TestState(tpCheckPoint, True,  Point(2, 0), 2, 0, '', 'Mxmo1'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'Mxxmo1'),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'Mxxmo1')]);

    TestBegin('code: replace-same');
    TestEdit.SelStart := 1;
    TestEdit.SelLength := 3;
    CheckPoint();
    TestEdit.SelText := 'emo';
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'M1'),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', 'Memo1'),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', 'Memo1')]);

    TestBegin('code: replace-maxlength');
    TestEdit.MaxLength := 7;
    TestEdit.SelStart := 0;
    TestEdit.SelLength := 0;
    CheckPoint();
    TestEdit.SelText := 'Memo';
    TestEdit.SelStart := 4;
    TestEdit.SelLength := 2;
    TestEdit.SelText := 'Memo';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'Memo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'MeMemo1'),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', 'MeMe1'),
      TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'MeMeMe1'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'MeMeMe1')]);
    TestEdit.MaxLength := 0;

    TestBegin('code: multiline', 'Line 1\nLine 2\nLine 3\n');
    TestEdit.SelStart := 0;
    TestEdit.SelLength := 0;
    CheckPoint();
    TestEdit.CaretPos := Point(2, 2);
    CheckPoint();
    TestEdit.SelLength := 3;
    CheckPoint();
    TestEdit.SelText := 'sel';
    TestEdit.CaretPos := Point(4, 1);
    TestEdit.SelText := 'sel';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0,             0, '', 'Line 1\nLine 2\nLine 3\n'),
      TestState(tpCheckPoint, False, Point(2, 2), 6+LBL+6+LBL+2, 0, '', 'Line 1\nLine 2\nLine 3\n'),
      TestState(tpCheckPoint, False, Point(5, 2), 6+LBL+6+LBL+2, 3, 'ne ', 'Line 1\nLine 2\nLine 3\n'),
      TestState(tpOnChange,   True,  Point(2, 2), 6+LBL+6+LBL+2, 0, '', 'Line 1\nLine 2\nLi3\n'),
      TestState(tpOnChange,   True,  Point(5, 2), 6+LBL+6+LBL+5, 0, '', 'Line 1\nLine 2\nLisel3\n'),
      TestState(tpOnChange,   True,  Point(7, 1), 6+LBL+7,       0, '', 'Line 1\nLinesel 2\nLisel3\n'),
      TestState(tpAfterTest,  True,  Point(7, 1), 6+LBL+7,       0, '', 'Line 1\nLinesel 2\nLisel3\n')]);


    FocusEdit(True);

    TestBegin('keyboard: insert');
    KeyboardSelect(0, 0);
    KeyboardPress(VK_A);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'aMemo1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'aMemo1')]);

    TestBegin('keyboard: replace');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardPress(VK_X);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'M1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Mx1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Mx1')]);

    TestBegin('keyboard: replace-same');
    KeyboardSelect(1, 1);
    CheckPoint();
    KeyboardPress(VK_E);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 1, 1, 'e', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Mmo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Memo1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Memo1')]);

    TestBegin('keyboard: backspace');
    KeyboardSelect(2, 0);
    CheckPoint();
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'Mmo1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'Mmo1')]);

    TestBegin('keyboard: backspace-selected');
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'mo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Me1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Me1')]);

    TestBegin('keyboard: delete');
    KeyboardSelect(2, 0);
    CheckPoint();
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpCheckPoint, False, Point(2, 0), 2, 0, '', 'Memo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Meo1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Meo1')]);

  TestBegin('keyboard: delete-selected');
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'mo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Me1'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'Me1')]);

    TestBegin('keyboard: cut');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_X);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'M1'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'M1')]);

    TestBegin('keyboard: copy-paste');
    KeyboardSelect(1, 3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(3, 0);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpCheckPoint, False, Point(3, 0), 3, 0, '', 'Memo1'),
      TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'Mememoo1'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'Mememoo1')]);

    TestBegin('keyboard: copy-paste-selected');
    KeyboardSelect(4, -3);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(2, 2);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(4, 0), 1, 3, 'emo', 'Memo1'),
      TestState(tpCheckPoint, False, Point(4, 0), 2, 2, 'mo', 'Memo1'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'Me1'),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', 'Meemo1'),
      TestState(tpAfterTest,  True,  Point(5, 0), 5, 0, '', 'Meemo1')]);

    TestBegin('keyboard: copy-paste-selected-all');
    KeyboardSelect(0, 5);
    KeyboardShortcut([ssCtrl], VK_C);
    CheckPoint();
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpCheckPoint, False, Point(5, 0), 0, 5, 'Memo1', 'Memo1'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', ''),
      TestState(tpOnChange,   True,  Point(5, 0), 5, 0, '', 'Memo1'),
      TestState(tpAfterTest,  True,  Point(5, 0), 5, 0, '', 'Memo1')]);

    TestBegin('keyboard: copy-paste-maxlength');
    TestEdit.MaxLength := 7;
    KeyboardSelect(0, 4);
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(0, 0);
    KeyboardShortcut([ssCtrl], VK_V);
    KeyboardSelect(4, 2);
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'MeMemo1'),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', 'MeMe1'),
      TestState(tpOnChange,   True,  Point(6, 0), 6, 0, '', 'MeMeMe1'),
      TestState(tpAfterTest,  True,  Point(6, 0), 6, 0, '', 'MeMeMe1')]);
    TestEdit.MaxLength := 0;


    FocusEdit(True);

    TestBegin('issue-32583', 'abcdefg');
    CheckPoint();
    for i := 1 to 5 do begin
      TestEdit.SelStart := 1;
      TestEdit.SelLength := 2;
      TestEdit.SelText := 'a';
    end;
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'abcdefg'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'adefg'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'aadefg'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'aefg'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'aaefg'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'afg'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'aafg'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'ag'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'aag'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'a'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'aa'),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', 'aa')]);

    TestBegin('issue-32583-2', 'abc');
    TestEdit.SelStart := 0;
    TestEdit.SelLength := 1;
    CheckPoint();
    TestEdit.SelText := 'd';
    TestEnd([
      TestState(tpCheckPoint, False, Point(1, 0), 0, 1, 'a', 'abc'),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', 'bc'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'dbc'),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', 'dbc')]);

    TestBegin('issue-32583-3', 'abcd');
    CheckPoint();
    TestEdit.SelStart := 1;
    TestEdit.SelLength := 1;
    TestEdit.SelText := 'x';
    CheckPoint();
    TestEdit.SelText := 'x';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', 'abcd'),
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', 'acd'),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', 'axcd'),
      TestState(tpCheckPoint, True,  Point(2, 0), 2, 0, '', 'axcd'),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', 'axxcd'),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', 'axxcd')]);

    TestBegin('issue-33225', '1234<>7890');
    CheckPoint();
    TestEdit.SelStart := 5;
    TestEdit.SelLength := 0;
    TestEdit.SelText := 'aöçe';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', '1234<>7890'),
      TestState(tpOnChange,   True,  Point(9, 0), 9, 0, '', '1234<aöçe>7890'),
      TestState(tpAfterTest,  True,  Point(9, 0), 9, 0, '', '1234<aöçe>7890')]);

    TestBegin('issue-37166-1', '0\n1\n2\n3\n4\n5\n6\n\n\n9\n');
    CheckPoint();
    i := TMemo(TestEdit).Lines.Count - 1;
    TMemo(TestEdit).Lines[i] := TMemo(TestEdit).Lines[i] + 'Hello';
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0),   0, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n9\n'),
      TestState(tpOnChange,   True,  Point(0, 9),  25, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n'),
      TestState(tpOnChange,   True,  Point(0, 10), 33, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n9Hello\n'),
      TestState(tpAfterTest,  True,  Point(0, 10), 33, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n9Hello\n')]);

    TestBegin('issue-37166-2', '0\n1\n2\n3\n4\n5\n6\n\n\n9\n');
    CheckPoint();
    i := TMemo(TestEdit).Lines.Count - 1;
    TMemo(TestEdit).Lines.Delete(i);
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0),  0, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n9\n'),
      TestState(tpOnChange,   True,  Point(0, 9), 25, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n'),
      TestState(tpAfterTest,  True,  Point(0, 9), 25, 0, '', '0\n1\n2\n3\n4\n5\n6\n\n\n')]);
  end;
end;

procedure TForm1.SpinEditRunTests();
  function fmt(v: Real; dp: Integer = -1): string;
  begin
    if( dp = -1 )then
      dp := TFloatSpinEdit(FTester.TestEdit).DecimalPlaces;
    Exit(Format('%.*f', [dp, v]));
  end;
var
  d: Integer;
begin
  with( FTester )do begin
    FocusEdit(True);

    SetSpinReset(1, -100, 100, 2);

    for d := 0 to 2 do begin
      ResetDecimalPlaces := d;

      TestBegin('code: set / dp=' + IntToStr(d));
      TFloatSpinEdit(TestEdit).Value := -8;
      TestEnd([
        TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(-8), -8),
        TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(-8), -8)]);

      TestBegin('code: set-over / dp=' + IntToStr(d));
      TFloatSpinEdit(TestEdit).Value := 800;
      TestEnd([
        TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(100), 100),
        TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(100), 100)]);

      TestBegin('keyboard: up / dp=' + IntToStr(d));
      KeyboardPress(VK_UP);
      TestEnd([
        TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', fmt(1), 1),
        TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', fmt(1), 1)]);

      TestBegin('keyboard: down / dp=' + IntToStr(d));
      KeyboardPress(VK_DOWN);
      TestEnd([
        TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', fmt(-1), -1),
        TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', fmt(-1), -1)]);

      TestBegin('keyboard: insert / dp=' + IntToStr(d));
      KeyboardSelect(0, 0);
      KeyboardPress(VK_8);
      TestEnd([
        TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', fmt(80), 80),
        TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', fmt(80), 80)]);

      TestBegin('keyboard: insert-extra / dp=' + IntToStr(d), fmt(1));
      KeyboardSelect(1, 0);
      KeyboardPress(VK_2);
      KeyboardPress(VK_3);
      KeyboardPress(VK_4);
      TestEnd([
        TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(12),   12),
        TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', fmt(123),  100),
        TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', fmt(1234), 100),
        TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', fmt(1234), 100)]);

      TestBegin('keyboard: replace / dp=' + IntToStr(d));
      KeyboardSelect(0, Length(TestEdit.Text));
      KeyboardPress(VK_8);
      TestEnd([
        TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '8', 8),
        TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', '8', 8)]);
    end;

    SetSpinReset(1, -100, 100, 2);

    TestBegin('set-decimalplaces');
    TFloatSpinEdit(TestEdit).DecimalPlaces := 3;
    TestEnd([
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', fmt(0), 0),
      TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', fmt(0), 0)]);

    TestBegin('set-from-onchange');
    TestOnChange := @OnChangeSpinSet;
    KeyboardSelect(0, 0);
    KeyboardPress(VK_1);
    TFloatSpinEdit(TestEdit).Value := 20.0;
    TestOnChange := nil;
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', fmt(10), 10),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(5), 5),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(20), 20),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(5), 5),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(5), 5)]);

    TestBegin('keyboard: delete', fmt(1.23));
    KeyboardSelect(2, 0);
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '1.3', 1.3),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', '1.3', 1.3)]);

    TestBegin('keyboard: backspace', fmt(1.23));
    KeyboardSelect(4, 0);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '1.2', 1.2),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', '1.2', 1.2)]);

    TestBegin('keyboard: copy-paste', fmt(1.23));
    KeyboardSelect(2, 2);
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardSelect(0, 1);
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(23.23), 23.23),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', fmt(23.23), 23.23)]);

    TestBegin('keyboard: copy-paste-all', fmt(1.23));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardShortcut([ssCtrl], VK_C);
    KeyboardShortcut([ssCtrl], VK_V);
    TestEnd([
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', fmt(1.23), 1.23),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', fmt(1.23), 1.23)]);

    SetSpinReset(1, 0, 100, 2);
    ResetEdit();
    with( TFSpinTest.Create(Application) )do
      try
        Show();
        SetFocus();
        FTester.TestEdit := FloatSpinEdit2;
        TestBegin('issue-9338', fmt(1.00));
        KeyboardSelect(1, 1);
        KeyboardPress(VK_0);
        KeyboardPress(VK_0);
      finally
        Free();
      end;
    FTester.TestEdit := FloatSpinEdit1;
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '1000',   100),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '10000',  100),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(0), 0)]);

    SetSpinReset(1, 0, 100, 2);

    TestBegin('issue-18679-1', fmt(1.00));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardPress(VK_9);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '9', 9),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', '9', 9)]);

    TestBegin('issue-18679-2', fmt(1.23));
    KeyboardSelect(0, 1);
    KeyboardPress(VK_4);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', fmt(4.23), 4.23),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', fmt(4.23), 4.23)]);

    TestBegin('issue-18679-3', fmt(1.23));
    KeyboardSelect(2, 1);
    KeyboardPress(VK_4);
    TestEnd([
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', fmt(1.43), 1.43),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', fmt(1.43), 1.43)]);

    TestBegin('issue-18679-4', fmt(1.23));
    KeyboardSelect(3, 1);
    KeyboardPress(VK_4);
    TestEnd([
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', fmt(1.24), 1.24),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', fmt(1.24), 1.24)]);

    TestBegin('issue-18679-5', fmt(1.23));
    KeyboardSelect(4, -1);
    KeyboardPress(VK_4);
    TestEnd([
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', fmt(1.24), 1.24),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', fmt(1.24), 1.24)]);

    TestBegin('issue-18679-6', fmt(1.00));
    KeyboardSelect(3, 0);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(1, 1), 1),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', fmt(1, 1), 1)]);

    TestBegin('issue-18679-7', fmt(2.00));
    KeyboardSelect(4, 0);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', fmt(2, 1), 2),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', fmt(2, 1), 2)]);

    TestBegin('issue-18679-8', fmt(3.00));
    KeyboardSelect(2, 1);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(3, 1), 3),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', fmt(3, 1), 3)]);

    TestBegin('issue-18679-9', fmt(4.00));
    KeyboardSelect(3, 1);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', fmt(4, 1), 4),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', fmt(4, 1), 4)]);

    TestBegin('issue-18679-a', fmt(0.30));
    KeyboardSelect(2, 0);
    KeyboardPress(VK_BACK);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '030', 30),
      TestState(tpAfterTest,  True,  Point(1, 0), 1, 0, '', '030', 30)]);

    TestBegin('issue-18679-b', fmt(0.00));
    KeyboardSelect(1, 0);
    KeyboardPress(VK_5);
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '0'+fmt(5), 5),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', '0'+fmt(5), 5)]);

    TestBegin('issue-18679-c', fmt(3.00));
    KeyboardSelect(0, 0);
    KeyboardPress(VK_DELETE);
    TestEnd([
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', '.00', 0),
      TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', '.00', 0)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-19801', fmt(0.00));
    TFloatSpinEdit(TestEdit).Value := TFloatSpinEdit(TestEdit).Value + 1.0;
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(1), 1),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(1), 1)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-20601');
    TestEdit.ReadOnly := True;
    KeyboardPress(VK_UP);
    KeyboardPress(VK_DOWN);
    TestEdit.ReadOnly := False;
    TestEnd([
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(0), 0)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-21224-1');
    TFloatSpinEdit(TestEdit).OnEditingDone := @OnEditingDone21224;
    ResetEdit(fmt(100.00));
    KeyboardSelect(1, 0);
    KeyboardPress(VK_DELETE);
    KeyboardPress(VK_RETURN);
    ResetEdit(fmt(100.00));
    KeyboardSelect(1, 0);
    KeyboardPress(VK_0);
    KeyboardPress(VK_RETURN);
    TFloatSpinEdit(TestEdit).OnEditingDone := nil;
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', fmt(10),   10),
      TestState(tpCheckPoint, True,  Point(1, 0), 1, 0, '', fmt(10),   10),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(1000), 100),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', fmt(100),  100),
      TestState(tpCheckPoint, True,  Point(0, 0), 0, 0, '', fmt(100),  100),
      TestState(tpAfterTest,  True,  Point(6, 0), 0, 6, fmt(100), fmt(100), 100)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-21224-2');
    TFloatSpinEdit(TestEdit).OnEditingDone := @OnEditingDone21224;
    ResetEdit(fmt(100.00));
    KeyboardSelect(1, 0);
    KeyboardPress(VK_0);
    edLog.SetFocus();
    TFloatSpinEdit(TestEdit).OnEditingDone := nil;
    TestEnd([
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', fmt(1000), 100),
      TestState(tpOnChange,   True,  Point(0, 0), 0, 0, '', fmt(100),  100),
      TestState(tpCheckPoint, True,  Point(0, 0), 0, 0, '', fmt(100),  100),
      TestState(tpAfterTest,  True,  Point(0, 0), 0, 0, '', fmt(100),  100)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-21638', fmt(1.00));
    edLog.SetFocus();
    TestEdit.SetFocus();
    edLog.SetFocus();
    TestEnd([
      TestState(tpAfterTest,  False, Point(4, 0), 0, 4, fmt(1), fmt(1), 1)]);

    SetSpinReset(0.1, 25.9, 59.9, 2);
    TestBegin('issue-23266', fmt(0.00));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardPress(VK_3);
    KeyboardPress(VK_0);
    KeyboardPress(VK_DECIMAL);
    KeyboardPress(VK_0);
    TestEnd([
      TestState(tpOnChange,   False, Point(1, 0), 1, 0, '', '3',  25.9),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '30',   30),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '30.',  30),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', '30.0', 30),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', '30.0', 30)]);

    SetSpinReset(1, 0, 0, 0);
    TestBegin('issue-25735', fmt(0));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardPress(VK_1);
    KeyboardPress(VK_2);
    KeyboardPress(VK_3);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '1',   1),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '12',  12),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '123', 123),
      TestState(tpAfterTest,  True,  Point(3, 0), 3, 0, '', '123', 123)]);

    SetSpinReset(1, 0, 100, 2);
    TestBegin('issue-26488', fmt(10.00));
    CheckPoint();
    TestEdit.Enabled := False;
    TestEdit.Clear();
    CheckPoint();
    TestEdit.Enabled := True;
    TFloatSpinEdit(TestEdit).Value := 10.0;
    TestEnd([
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', '10.00', 10),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', '',      10),
      TestState(tpCheckPoint, False, Point(0, 0), 0, 0, '', '',      10),
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', '10.00', 10),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', '10.00', 10)]);

    SetSpinReset(1, 12, 100, 0);
    TestBegin('issue-28123-1', fmt(20));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardPress(VK_3);
    KeyboardPress(VK_7);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '3',  12),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '37', 37),
      TestState(tpAfterTest,  True,  Point(2, 0), 2, 0, '', '37', 37)]);

    SetSpinReset(1, 1.2, 100, 2);
    TestBegin('issue-28123-2', fmt(3.4));
    KeyboardSelect(0, Length(TestEdit.Text));
    KeyboardPress(VK_1);
    KeyboardPress(VK_2);
    KeyboardPress(VK_DECIMAL);
    KeyboardPress(VK_3);
    TestEnd([
      TestState(tpOnChange,   True,  Point(1, 0), 1, 0, '', '1',    1.2),
      TestState(tpOnChange,   True,  Point(2, 0), 2, 0, '', '12',   12),
      TestState(tpOnChange,   True,  Point(3, 0), 3, 0, '', '12.',  12),
      TestState(tpOnChange,   True,  Point(4, 0), 4, 0, '', '12.3', 12.3),
      TestState(tpAfterTest,  True,  Point(4, 0), 4, 0, '', '12.3', 12.3)]);

    SetSpinReset(1, 0, 0, 2);
    TestBegin('issue-29645', fmt(0));
    TFloatSpinEdit(TestEdit).Value := 2.0;
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(2), 2),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(2), 2)]);

    SetSpinReset(1, 0, 100, 0);
    TestBegin('issue-32368-1', fmt(0));
    TFloatSpinEdit(TestEdit).Value := 2;
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(2), 2),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(2), 2)]);

    SetSpinReset(1, 0, 100, 1);
    TestBegin('issue-32368-2', fmt(0));
    TFloatSpinEdit(TestEdit).Value := 2;
    TestEnd([
      TestState(tpOnChange,   False, Point(0, 0), 0, 0, '', fmt(2), 2),
      TestState(tpAfterTest,  False, Point(0, 0), 0, 0, '', fmt(2), 2)]);

    SetSpinReset(1, -100, 100, 2);
    ResetEdit(fmt(1));
    ResetEdit(fmt(0));
  end;
end;

end.

