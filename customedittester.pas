unit CustomEditTester;

{$mode objfpc}{$H+}
{$modeswitch advancedrecords}

interface

uses
  SysUtils, Classes, StdCtrls, Spin;

const
  DefaultSpinEditValue = 0;

type

  { TTestState }

  TTestProbe = (tpOnChange, tpCheckPoint, tpAfterTest);
  TTestSubject = (tsTestProbe, tsModified, tsCaretPos, tsSelStart, tsSelLength, tsSelText, tsText, tsValue);
  TTestSubjects = set of TTestSubject;
  TTestState = record
    TestProbe: TTestProbe;
    Modified: Boolean;
    CaretPos: TPoint;
    SelStart: Integer;
    SelLength: Integer;
    SelText: string;
    Text: string;
    Value: Double;
    procedure Init(ATestProbe: TTestProbe; AModified: Boolean; ACaretPos: TPoint;
      ASelStart, ASelLength: Integer; const ASelText, AText: string; AValue: Double);
    function Diff(const S: TTestState; TS: TTestSubjects): TTestSubjects;
  end;

const
  AllTestSubjects = [Low(TTestSubject) .. High(TTestSubject)];

type

  { TCustomEditTester }

  TCustomEditTester = class
  protected type
    TLogProc = procedure (ASender: TCustomEditTester; const ALine: string) of object;
  protected
    FTestOnChange: TNotifyEvent;
    FTestEdit: TCustomEdit;
    FResetText: string;
    FResetIncrement: Real;
    FResetMinValue: Real;
    FResetMaxValue: Real;
    FResetDecimalPlaces: Integer;
    FSetFocus: Boolean;
    FTestSubjects: TTestSubjects;
    FKeyPressDelay: Integer;
    FOnLog: TLogProc;
    FStates: array of TTestState;
    FTestResult: array of Boolean;
    FLate: Boolean;
    procedure TestEditOnChange(Sender: TObject);
  public
    property TestOnChange: TNotifyEvent read FTestOnChange write FTestOnChange;
    property TestEdit: TCustomEdit read FTestEdit write FTestEdit;
    property ResetText: string read FResetText write FResetText;
    property ResetIncrement: Real read FResetIncrement write FResetIncrement;
    property ResetMinValue: Real read FResetMinValue write FResetMinValue;
    property ResetMaxValue: Real read FResetMaxValue write FResetMaxValue;
    property ResetDecimalPlaces: Integer read FResetDecimalPlaces write FResetDecimalPlaces;
    property SetFocus: Boolean read FSetFocus write FSetFocus;
    property TestSubjects: TTestSubjects read FTestSubjects write FTestSubjects;
    property KeyPressDelay: Integer read FKeyPressDelay write FKeyPressDelay;
    property OnLog: TLogProc read FOnLog write FOnLog;
  public
    constructor Create();
    procedure Start();
    procedure Log(const ALine: string);
    procedure Log(const AFmt: string; AArgs: array of const);
    procedure LogSummary();
    procedure KeyboardPress(AKey: Word; ACount: Integer = 1);
    procedure KeyboardSelect(AStart, ALength: Integer);
    procedure KeyboardShortcut(AShiftState: TShiftState; AKey: Word);
    procedure SetSpinReset(AIncrement, AMinValue, AMaxValue: Double; ADecimalPlaces: Integer);
    procedure ResetEdit(const AResetText: string);
    procedure ResetEdit();
    procedure RecordState(ATestProbe: TTestProbe);
    procedure CheckPoint();
    procedure TestBegin(const ATitle: string; const AResetText: string);
    procedure TestBegin(const ATitle: string);
    procedure TestEnd(AExpected: array of TTestState);
  end;

function TestState(ATestProbe: TTestProbe; AModified: Boolean; ACaretPos: TPoint;
  ASelStart, ASelLength: Integer; const ASelText, AText: string;
  AValue: Double = DefaultSpinEditValue): TTestState;

implementation

uses
  MouseAndKeyInput, Forms, LCLType, Math, TypInfo, LazUTF8;

{ TTestState }

function TestState(ATestProbe: TTestProbe; AModified: Boolean; ACaretPos: TPoint;
  ASelStart, ASelLength: Integer; const ASelText, AText: string; AValue: Double): TTestState;
begin
  Result.Init(ATestProbe, AModified, ACaretPos, ASelStart, ASelLength, ASelText, AText, AValue);
end;

procedure TTestState.Init(ATestProbe: TTestProbe; AModified: Boolean; ACaretPos: TPoint;
  ASelStart, ASelLength: Integer; const ASelText, AText: string; AValue: Double);
  function repl(const s: string): string;
  begin
    Result := s;
    Result := UTF8StringReplace(Result, sLineBreak, '\n', [rfReplaceAll]);
    Result := UTF8StringReplace(Result, ',', '.', [rfReplaceAll]);
  end;
begin
  Self.TestProbe := ATestProbe;
  Self.Modified := AModified;
  Self.CaretPos := ACaretPos;
  Self.SelStart := ASelStart;
  Self.SelLength := ASelLength;
  Self.SelText := repl(ASelText);
  Self.Text := repl(AText);
  Self.Value := AValue;
end;

function TTestState.Diff(const S: TTestState; TS: TTestSubjects): TTestSubjects;
begin
  Result := [];
  if( tsTestProbe in TS )and( Self.TestProbe <> S.TestProbe )then
    Include(Result, tsTestProbe);
  if( tsModified in TS )and( Self.Modified <> S.Modified )then
    Include(Result, tsModified);
  if( tsCaretPos in TS )and( not PointsEqual(Self.CaretPos, S.CaretPos) )then
    Include(Result, tsCaretPos);
  if( tsSelStart in TS )and( Self.SelStart <> S.SelStart )then
    Include(Result, tsSelStart);
  if( tsSelLength in TS )and( Self.SelLength <> S.SelLength )then
    Include(Result, tsSelLength);
  if( tsSelText in TS )and( Self.SelText <> S.SelText )then
    Include(Result, tsSelText);
  if( tsText in TS )and( Self.Text <> S.Text )then
    Include(Result, tsText);
  if( tsValue in TS )and( Self.Value <> S.Value )then
    Include(Result, tsValue);
end;

{ TCustomEditTester }

procedure TCustomEditTester.TestEditOnChange(Sender: TObject);
begin
  RecordState(tpOnChange);
  if( Assigned(FTestOnChange) )then
    FTestOnChange(FTestEdit);
end;

constructor TCustomEditTester.Create();
begin
  FOnLog := nil;
  FTestOnChange := nil;
  FTestEdit := nil;
  FResetText := '';
  SetSpinReset(1.0, 0.0, 100.0, 2);
  FSetFocus := True;
  FTestSubjects := AllTestSubjects;
  KeyPressDelay := 0;
  SetLength(FStates, 0);
end;

procedure TCustomEditTester.Start();
begin
  SetLength(FTestResult, 0);
  Log('[--------------]');
end;

procedure TCustomEditTester.Log(const ALine: string);
begin
  if( Assigned(FOnLog) )then
    FOnLog(Self, ALine);
end;

procedure TCustomEditTester.Log(const AFmt: string; AArgs: array of const);
begin
  Log(Format(AFmt, AArgs));
end;

procedure TCustomEditTester.LogSummary();
var
  I, F1, F2, Passed: Integer;
  Failed: string;
  procedure AddF();
  begin
    if( F1 = -1 )then
      Exit();
    if( F1 = F2 )then
      Failed := Failed + Format('%d, ', [F1 + 1])
    else if( F2 - F1 = 1 )then
      Failed := Failed + Format('%d, %d, ', [F1 + 1, F2 + 1])
    else
      Failed := Failed + Format('%d-%d, ', [F1 + 1, F2 + 1]);
    F1 := -1;
  end;
begin
  Passed := 0;
  Failed := '';
  F1 := -1;
  for I := 0 to Length(FTestResult) - 1 do begin
    if( FTestResult[I] )then begin
      AddF();
      Inc(Passed);
    end else if( F1 = -1 )then begin
      F1 := I;
      F2 := I;
    end else
      F2 := I;
  end;
  AddF();
  Log('[      Summary ] Total: %d, Passed: %d, Failed: %d',
    [Length(FTestResult), Passed, Length(FTestResult) - Passed]);
  if( Failed <> '' )then begin
    Delete(Failed, Length(Failed) - 1, 2);
    Log('[ Failed Tests ] %s', [Failed]);
  end;

  Log('[--------------]');
end;

procedure TCustomEditTester.KeyboardPress(AKey: Word; ACount: Integer);
var
  I: Integer;
begin
  for I := 0 to ACount - 1 do
    KeyInput.Press(AKey);
  for I := 0 to KeyPressDelay div 10 - 1 do begin
    Sleep(10);
    Application.ProcessMessages();
  end;
end;

procedure TCustomEditTester.RecordState(ATestProbe: TTestProbe);
var
  L: Integer;
  N, S: string;
  Value: Double;
begin
  L := Length(FStates);
  SetLength(FStates, L + 1);
  with( FTestEdit )do begin
    if( FTestEdit is TFloatSpinEdit )then
      Value :=(FTestEdit as TFloatSpinEdit).Value
    else
      Value := DefaultSpinEditValue;
    FStates[L].Init(ATestProbe, Modified, CaretPos, SelStart, SelLength, SelText, Text, Value);
    N := Copy(GetEnumName(TypeInfo(TTestProbe), Ord(ATestProbe)), 3, MaxInt);
    S := '';
    with FStates[L] do begin
      if( tsTestProbe in FTestSubjects )then
        S := S + Format('[%2d:%-11s]', [L + 1, N]);
      if( tsModified  in FTestSubjects )then
        S := S + Format(' Modified=%-5s', [BoolToStr(Modified, True)]);
      if( tsCaretPos  in FTestSubjects )then
        S := S + Format(' CaretPos=(%d,%d)', [CaretPos.X, CaretPos.Y]);
      if( tsSelStart  in FTestSubjects )then
        S := S + Format(' SelStart=%-2d', [SelStart]);
      if( tsSelLength in FTestSubjects )then
        S := S + Format(' SelLength=%-2d', [SelLength]);
      if( tsSelText   in FTestSubjects )then
        S := S + Format(' SelText="%s"', [SelText]);
      if( tsText      in FTestSubjects )then
        S := S + Format(' Text="%s"', [Text]);
      if( tsValue     in FTestSubjects )then
        S := S + Format(' Value=%.4f', [Value]);
    end;
    if( FLate )then begin
      FLate := False;
      Log('[  **  **  **  ]');
    end;
    Log(S);
  end;
end;

procedure TCustomEditTester.CheckPoint();
begin
  RecordState(tpCheckPoint);
end;

procedure TCustomEditTester.TestBegin(const ATitle: string; const AResetText: string);
var
  TestIndex: Integer;
begin
  FLate := False;
  ResetEdit(AResetText);
  SetLength(FStates, 0);
  TestIndex := Length(FTestResult);
  SetLength(FTestResult, TestIndex + 1);
  Log('[      Test %2d ] ** %s **', [TestIndex + 1, ATitle]);
end;

procedure TCustomEditTester.TestBegin(const ATitle: string);
begin
  TestBegin(ATitle, FResetText);
end;

procedure TCustomEditTester.KeyboardSelect(AStart, ALength: Integer);
begin
  KeyboardShortcut([], VK_HOME);
  KeyboardShortcut([ssCtrl], VK_HOME);
  KeyboardPress(VK_RIGHT, AStart);
  if( ALength <> 0 )then begin
    KeyInput.Apply([ssShift]);
    if( ALength > 0 )then
      KeyboardPress(VK_RIGHT, ALength)
    else if( ALength < 0 )then
      KeyboardPress(VK_LEFT, -ALength);
    KeyInput.Unapply([ssShift]);
  end;
end;

procedure TCustomEditTester.KeyboardShortcut(AShiftState: TShiftState; AKey: Word);
begin
  KeyInput.Apply(AShiftState);
  KeyInput.Press(AKey);
  KeyInput.Unapply(AShiftState);
end;

procedure TCustomEditTester.SetSpinReset(AIncrement, AMinValue, AMaxValue: Double; ADecimalPlaces: Integer);
begin
  FResetIncrement := AIncrement;
  FResetMinValue := AMinValue;
  FResetMaxValue := AMaxValue;
  FResetDecimalPlaces := ADecimalPlaces;
end;

procedure TCustomEditTester.ResetEdit(const AResetText: string);
begin
  FTestEdit.OnChange := nil;
  if( FSetFocus )then
    FTestEdit.SetFocus();
  if( FTestEdit is TFloatSpinEdit )then begin
    (FTestEdit as TFloatSpinEdit).Increment := FResetIncrement;
    (FTestEdit as TFloatSpinEdit).MinValue := FResetMinValue;
    (FTestEdit as TFloatSpinEdit).MaxValue := FResetMaxValue;
    (FTestEdit as TFloatSpinEdit).DecimalPlaces := FResetDecimalPlaces;
    (FTestEdit as TFloatSpinEdit).Value :=
      StrToFloat(UTF8StringReplace(AResetText, '.', DefaultFormatSettings.DecimalSeparator, [rfReplaceAll]));
  end else
    FTestEdit.Text := UTF8StringReplace(AResetText, '\n', sLineBreak, [rfReplaceAll]);
  FTestEdit.SelStart := 0;
  FTestEdit.SelLength := 0;
  FTestEdit.CaretPos := Point(0, 0);
  FTestEdit.Modified := False;
  FTestEdit.OnChange := @TestEditOnChange;
end;

procedure TCustomEditTester.ResetEdit();
begin
  ResetEdit(FResetText);
end;

procedure TCustomEditTester.TestEnd(AExpected: array of TTestState);
var
  I: Integer;
  FailOn: Integer;
  Diff: TTestSubjects;
  TS: TTestSubject;
  S, N: string;
begin
  RecordState(tpAfterTest);
  FailOn := -1;
  for I := 0 to Math.Min(Length(AExpected), Length(FStates)) - 1 do begin
    Diff := FStates[I].Diff(AExpected[I], FTestSubjects);
    if( Diff <> [] )then begin
      FailOn := I;
      Break;
    end;
  end;
  S := '';
  if( FailOn <> -1 )then begin
    for TS in TTestSubject do
      if( TS in Diff )then begin
        N := Copy(GetEnumName(TypeInfo(TTestSubject), Ord(TS)), 3, MaxInt);
        S := S + N + ', ';
      end;
    Delete(S, Length(S) - 1, 2);
    S := Format('%d: %s', [FailOn + 1, S] );
  end;
  I := Length(FStates);
  if( I <> Length(AExpected) )then
    FailOn := $FF01;
  FLate := True;
  Application.ProcessMessages();
  if( I <> Length(FStates) )then
    FailOn := $FF02;
  if( FailOn > $FF00 )then begin
    if( S <> '' )then
      S := S + ' / ';
    case( FailOn )of
      $FF01: S := S + 'Probe count';
      $FF02: S := S + 'Late probe';
    end;
  end;
  if( S <> '' )then
    S := Format('  (%s)', [S] );
  FTestResult[Length(FTestResult) - 1] := ( FailOn = -1 );
  Log('[         Pass ] %s%s', [UpperCase(BoolToStr(FailOn = -1, True)), S]);
  Log('[--------------]');
end;

end.

